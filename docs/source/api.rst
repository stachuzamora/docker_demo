Rest API - endpointy aplikacji
================================
1.
        * *localhost:8080/*
        Endpoint "witający" - główna strona wyświetla napis *Hello Docker World*
        Metoda: *GET*
     
2.
        * *localhost:8080/customers*
        Endpoint zwracający listę (iterator) wszystkich obiektów klasy Customer (klientów) dostępnych w bazie danych.
        Metoda: *GET*

3.
        * *localhost:8080/customer/{customerId}*
        Endpoint zwracający klienta o zadanym id w miejscu parametru *customerId*
        Metoda: *GET*



