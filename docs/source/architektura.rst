-------------------------
Architektura projektu
-------------------------

.. uml::
  
   Customer -> CustomerController: zapytanie GET '/customers'
   CustomerController -> CustomerRepository: wywołanie findAll()
   CustomerRepository -> CustomerController: zwraca listę obiektów klasy Customer
   CustomerController -> Customer: wyświetlanie listy klientów po stronie przeglądarki 

.. uml::

    Customer -> CustomerController: zapytanie GET '/customers/{customerId}'
    CustomerController -> CustomerRepository: wywołanie findById(customerId)
    CustomerRepository -> CustomerController: zwracanie obiektu klasy Customer
    CustomerController -> Customer: wyświetlenie Customera lub w przypadku braku takiego zwrócenie status NOT_FOUND


