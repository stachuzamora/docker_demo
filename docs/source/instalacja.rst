Uruchomienie aplikacji za pomocą Docker-compose
=================================================

Wymagania
--------------
Aby móc uruchomić aplikację z wykorzystaniem docker-compose, należy najpierw mieć zainstalowane docker engine oraz docker-compose.
Jest to wymagany krok, bez silnika dockerowego nie ma możliwości uruchomienia kontenerów. Pozostałe fragmenty (rozszerzenia, aplikacje, biblioteki) zostaną 
zaciągnięte odpowiednio zgodnie z konfiguracją użytkownika w docker-compose.yml.

Docker-compose.yml
---------------------
Docker compose jest plikiem wykorzystywanym do uruchomienia i zbudowania wielu aplikacji jednocześnie, tworząc *kompozycję*. Jego struktura jest ściśle określona, oprócz podania na początku wersji, należy zdefiniować serwisy (services), które są niczym innym, jak aplikacjami, które mają zostać zawarte w kompozycji. Na przykładzie aplikacji z laboratorium, kompozycja składa się z dwóch aplikacji - aplikacji i bazy danych. Pierwszym krokiem konfigurowania serwisu jest podanie nazwy obrazu, jaki ma zostać zbudowany. Jeśli budowany serwis został stworzony przy pomocy Dockera z wykorzystaniem Dockerfile'a, należy określić to w pliku compose z odpowiednimi wcięciami - podaje się kontekst do zbudowania oraz nazwę konfiguracyjnego pliku Dockerfile (ponieważ nie musi być to domyślna nazwa). Następnie należy podać zmienne środowisko, wykorzystane do autoryzacji użytkownika w aplikacji, zawarte w pliku należącym do katalogu .evn. Ostatnim krokiem jest skonfigurowanie portów. Docker umożliwia wystawienie aplikacji na innym porcie, niż domyślna implementacja w aplikacji. Składnia takiej konstrukcji wyglaga tak: <portOut>:<portIn>, gdzie portOut to port, na którym aplikacja zostanie wystawiona przez Dockera, a portIn jest portem, na którym aplikacja jest wewnętrznie wystawiana. W ten sam sposób buduje się składnik bazy danych - jako obraz podaje się bazę, która użytkownik chce wykorzsytać, porty wystawia się w ten sam sposób jak w przypadku aplikacji oraz podaje się zmienne środowiskowe zaciągnięte z pliku .env. Dodatkowo podać należy konfigurację *volumes*, czyli umożlwienie zapisywania plików w katalogu poza kontenerem. 

