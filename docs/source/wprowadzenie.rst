--------------------------
Docker i docker-compose
--------------------------

Docker
===========
1. Docker jest oprogramowaniem pozwalającym na tworzenie wirtualnych kontenerów przechowujących i pozwalajcycych
na uruchomienie dowolnej aplikacji (serwera, webowej, bazy danych czy nawet systemu operacyjnego). Ułatwia to przekazywanie 
plików i kodów wg. odgórnie ustalonych zasad, dzięki czemu każdy użytkownik posiadający dockera na swoim urządzeniu powinien
bez ingerencji w konfigurację  móc wszystko uruchomić.


Docker-compose
==================
2. Jest "rozszerzeniem" dockera pozwalającym tworzyć wielo-kontenerowe aplikacje dockerowe. W odróżnieniu od pojedycznego kontenera 
budowanego w Dockerfile, docker-compose wykorzystuje plik konfiguracyjny .yml, w którym skonfigurowane są tzw. serwisy (ang. services) 
, co pozwala na zbudowanie struktury uruchamianej jedną komendą. Przyświeca on idei mikro-serwisów, dzięki ci czemu jest wykorzsytywany 
"on a daily basis" 
