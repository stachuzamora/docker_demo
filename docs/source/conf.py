# Options for HTML output ---------------------------------------------------------

# The theme to use for HTML and HTML Help pages. See the documentation for 
# a list od built-in themes.
#
html_theme = 'sphinx_rtd_theme'
extensions = ['sphinxcontrib.plantuml']
plantuml = 'java -jar /path/to/plantuml.jar'
