FROM gradle:6.4.0-jdk8 AS build
USER root
RUN mkdir app
COPY src/ /app/src
COPY build.gradle /app/
COPY settings.gradle /app/
WORKDIR /app
RUN gradle bootJar

FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=build/libs/*.jar
COPY --from=build /app/build/libs/app.jar app.jar
#COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]

FROM postgres
COPY initdb/docker-entrypoint-initdb.d /docker-entrypoint-initdb.d